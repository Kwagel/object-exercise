import java.util.ArrayList;

public class Main {
    
    public static void main(String[] args) {
        Pet marvin = new Pet("Dynner", "Dog");
        
        Person bob = new Person("Bob", "Smith","male");
        Person sarah = new Person("Sarah", "Smith", "female");
        
        System.out.println(bob.getFullName());
        System.out.println(sarah.getFullName());
        
        sarah.setMyPet(marvin);
        sarah.setSurname("Jones");
        System.out.println(sarah.getFullName());
        
        Pet theirPet = sarah.getMyPet();
        if (theirPet == null) {
            System.out.println(sarah.getFullName() + " has no pet");
        } else {
            System.out.println(sarah.getFullName() + " has a " + theirPet.getSpecies().toLowerCase() + " called " +
                    theirPet.getName());
        }
        
        Person kenneth = new Person("Kenneth", " Lau", "male");
        Person hillary = new Person("Hillary", " Duff", "female");
    
        ArrayList<Person> residents = new ArrayList<>();
        residents.add(hillary);
        residents.add(bob);
        House home = new House(kenneth,residents,"NE1 7EW");
        System.out.println(home);
    }
    
}
