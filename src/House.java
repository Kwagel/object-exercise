import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class House {
    private Person Owner;
    private ArrayList<Person> residents = new ArrayList<>();
    private String postcode;
    
    public House(Person owner, ArrayList<Person> people, String postcode) {
        this.Owner = owner;
        this.residents.addAll(people);
        this.postcode = postcode;
    }
    
    public Person getOwner() {
        return Owner;
    }
    
    public void setOwner(Person owner) {
        Owner = owner;
    }
    
    public ArrayList<Person> getResidents() {
        return residents;
    }
    
    public void addResident(Person person) {
        this.residents.add(person);
    }
    public void removeResident(Person person) {
        this.residents.remove(person);
    }
    
    public String getPostcode() {
        return postcode;
    }
    
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }
    
    @Override
    public String toString() {
        return "House{" + "Owner=" + Owner + ", residents=" + residents + ", postcode='" + postcode + '\'' + '}';
    }
    
}
