public class Person {
    //    attributes
    private String firstName;
    private String surname;
    private String gender;
    private Pet myPet;
    
    public Person(String firstName, String surname, String gender) {
        this.firstName = firstName;
        this.surname = surname;
        this.gender = gender;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public String getSurname() {
        return surname;
    }
    
    public String getFullName() {
        return firstName + " " + surname;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public void setSurname(String surname) {
        this.surname = surname;
    }
    
    public void setMyPet(Pet myPet) {
        this.myPet = myPet;
    }
    
    public Pet getMyPet() {
        return myPet;
    }
    
    @Override
    public String toString() {
        return getFullName();
    }
    
}
